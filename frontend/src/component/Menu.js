import React from 'react';
import MenuItem from './MenuItem';
import { withRouter } from "react-router-dom";


const Menu = props => {

    return (
        <div className="flex flex-col m-32 max-w-xs">
            <MenuItem text="Oeuvres d'art" active={props.activeItem === "works"}  onClick={() => props.history.push("/works")}/>
            <MenuItem text="Artistes" active={props.activeItem === "artists"}  onClick={() => props.history.push("/artists")}/>
            <MenuItem text="Musées" active={props.activeItem === "museums"}  onClick={() => props.history.push("/museums")}/>
            <MenuItem text="Statistiques" active={props.activeItem === "stats"}  onClick={() => props.history.push("/stats")}/>
        </div>
    );
}

export default withRouter(Menu);
