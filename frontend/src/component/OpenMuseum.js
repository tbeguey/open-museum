import React from 'react';
import Menu from './Menu';
import Header from './Header'
import MyDataTable from './MyDataTable'
import Home from './Home';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
} from "react-router-dom";
import Stats from "./Statistique";

const columnsWorks = [
    {
        name: 'Titre',
        selector: 'title',
        sortable: true,
    },
    {
        name: 'Auteur',
        cell: row => <Link to={"/works/author/" + row.author.id} className="text-blue-600 hover:underline">{row.author.name}</Link>,
        sortable: true,
    },
    {
        name: 'Musée',
        cell: row => <Link to={"/works/museum/" + row.exhibition_museum.id} className="text-blue-600 hover:underline">{row.exhibition_museum.name}</Link>,
        sortable: true,
    },
];

const columnsArtists = [
    {
        name: 'Nom',
        cell: row => <Link to={"/works/author/" + row.id} className="text-blue-600 hover:underline">{row.name}</Link>,
        sortable: true,
    },
];


const columnsMuseums = [
    {
        name: 'Nom',
        cell: row => <Link to={"/works/musea/" + row.id} className="text-blue-600 hover:underline">{row.name}</Link>,
        sortable: true,
    },
    {
        name: 'Ville',
        selector: 'city.name',
        sortable: true,
    },

];

const OpenMuseum = props => {
    return (
        <Router>
            <Header />
            <div className="flex">
                <Switch>
                    <Route exact path="/">
                        <Home />
                    </Route>
                    <Route path="/works/author/:id">
                        <Menu activeItem="works" />
                        <MyDataTable columns={columnsWorks} url="artworks/?author=" />
                    </Route>
                    <Route path="/works/museum/:id">
                        <Menu activeItem="works" />
                        <MyDataTable columns={columnsWorks} url="artworks/?museum=" />
                    </Route>

                    <Route path="/works">
                        <Menu activeItem="works" />
                        <MyDataTable columns={columnsWorks} url="artworks/?" />
                    </Route>
                    <Route path="/artists">
                        <Menu activeItem="artists" />
                        <MyDataTable columns={columnsArtists} url="authors/?" />
                    </Route>
                    <Route path="/museums">
                        <Menu activeItem="museums" />
                        <MyDataTable columns={columnsMuseums} url="musea/?" />
                    </Route>
                    <Route path="/stats">
                        <Menu activeItem="stats" />
                        <Stats url="artworks" />
                    </Route>
                </Switch>
            </div>
        </Router>
    );
}

export default OpenMuseum;
