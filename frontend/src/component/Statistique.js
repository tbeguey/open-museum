import React, {useState, useEffect} from 'react';
import {Doughnut} from 'react-chartjs-2';
import Loader from "./Loader";

const Stats = props => {
    const [oeuvreCount, setDatacnt] = useState([]);
    const [museaCount, setMuseaCount] = useState([]);
    const [authorsCount, setAuthorsCount] = useState([]);
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        fetchDatas([]);
    }, []);

    const fetchDatas = async props => {
        setLoading(true);
        fetch("http://127.0.0.1:8000/artworks")
            .then(response => response.json())
            .then((oeuvreCount) => {
                setDatacnt(oeuvreCount.length);
                setLoading(false);
            });
        fetch("http://127.0.0.1:8000/musea")
            .then(response => response.json())
            .then((museaCount) => {
                setMuseaCount(museaCount.length);
            });
        fetch("http://127.0.0.1:8000/authors")
            .then(response => response.json())
            .then((authorsCount) => {
                setAuthorsCount(authorsCount.length);
            });
    };
    const data = {
        labels: [
            'Oeuvres',
            'Musées',
            'Artistes',
        ],
        datasets: [{
            data: [oeuvreCount, museaCount, authorsCount],
            backgroundColor: [
                '#FF6384',
                '#36A2EB',
                '#FFCE56'
            ],
            hoverBackgroundColor: [
                '#FF6384',
                '#36A2EB',
                '#FFCE56'
            ]
        }]
    };

    return (
        <div className="w-4/6">
            <div className="my-10 ">
                <h1 className="text-3xl text-blue-600">Statistiques de nos musées, artistes et oeuvres référencées</h1>
            </div>
            <div className="flex relative">
                <Doughnut
                    data={data}
                />
                {loading && <div className="absolute" style={{left: "48%", top: "50%"}}>
                    <Loader />
                </div>}
            </div>
        </div>
    );
};

export default Stats;