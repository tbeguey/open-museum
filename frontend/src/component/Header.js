import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faLandmark } from '@fortawesome/free-solid-svg-icons'
import { withRouter } from "react-router-dom";

const Header = props => {
  return (
    <div className="my-4 mx-16 w-1/6">
      <div className="text-4xl text-yellow-600 cursor-pointer" onClick={() => props.history.push("/")}>
        <span className="mx-4">OpenMuseum</span>
        <FontAwesomeIcon icon={faLandmark} />
      </div>
    </div>
  );
}

export default withRouter(Header);
