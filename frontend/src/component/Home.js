import React from 'react'
import { withRouter } from "react-router-dom";


const Home = props => {
    return (
        <div className="container mx-auto flex flex-col justify-center my-48">
            <span className="text-center text-4xl">
                Trouvez toutes les oeuvres de nos musées français sur OpenMuseum !
            </span>
            <div className="flex flex-col text-center text-2xl my-16">
                <span>
                    OpenMuseum vous permets d'organiser plus facilement vos sorties culturels en localisant
                </span>
                <span>
                    où se situe les artistes et oeuvres que vous voulez découvrir ou redécouvrir
                </span>
            </div>
            <button className="bg-yellow-600 hover:bg-yellow-700 text-3xl text-gray-300 py-8
             font-bold py-2 px-4 rounded-full cursor-pointer" onClick={() => props.history.push('/works')}>
                Découvrer les artistes et oeuvres exposés dans notre patrimoine culturel ...
            </button>
        </div>
    )
};

export default withRouter(Home);