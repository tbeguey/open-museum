import React from "react";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faSpinner} from "@fortawesome/free-solid-svg-icons";

const Loader = () => {
    return <FontAwesomeIcon className="rotating text-4xl" icon={faSpinner} />
};

export default Loader;