import React from 'react';

const MenuItem = props => {
  return (
        <div className={`my-4 text-3xl text-blue-500 cursor-pointer 
        ${props.active ? 'font-bold text-blue-600' : ' hover:underline'} `} onClick={props.onClick}>
            {props.text}
        </div>
  );
}

export default MenuItem;
