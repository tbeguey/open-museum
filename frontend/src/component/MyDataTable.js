import React, { useState, useEffect } from 'react';
import DataTable from 'react-data-table-component';
import {withRouter, useParams} from 'react-router-dom'

const MyDataTable = props => {
    const [data, setData] = useState([]);
    const [totalRows, setTotalRows] = useState(0);
    const [perPage, setPerPage] = useState(10);
    const [page, setPage] = useState(1);
    const [search, setSearch] = useState("");

    useEffect(() => {
        fetchDatas(page);
    }, []);

    useEffect(() => {
        setPage(1);
        setSearch("");
        fetchDatas(page);
    }, [props.url]);

    const fetchDatas = async page => {
        console.log(props);
        fetch("http://127.0.0.1:8000/" + props.url + props.match.params.id + "&limit=" + perPage 
        + "&offset=" + (page - 1) * perPage + "&search=" + search)
            .then(response => response.json())
            .then((data) => {
                setData(data.results);
                setTotalRows(data.count);
            });
    };

    const handlePageChange = newPage => {
        setPage(page);
        fetchDatas(newPage);
    };

    const handlePerRowsChange = async (newPerPage, newPage) => {
        setPerPage(newPerPage);
        setPage(newPage);

        fetch("http://127.0.0.1:8000/" + props.url + props.match.params.id + "&limit=" + newPerPage 
        + "&offset=" + (newPage - 1) * perPage + "&search=" + search)
            .then(response => response.json())
            .then((data) => {
                setData(data.results);
                setTotalRows(data.count);
            });
    };

    const handleSearch = (e) => {
        if (e.key === 'Enter') {
            setSearch(e.target.value);
            fetch("http://127.0.0.1:8000/" + props.url + props.match.params.id + "&limit=" + perPage 
            + "&offset=" + (page - 1) * perPage + "&search=" + e.target.value)
                .then(response => response.json())
                .then((data) => {
                    setData(data.results);
                    setTotalRows(data.count);
                });
        }
    };

    return (
        <div className="w-4/6">
            <div className="w-full flex justify-end">
                <input type="text" className="w-1/4 py-2 px-8 flex rounded-full" 
                onKeyDown={handleSearch} placeholder="Rechercher..." value={search} onChange={(e) => setSearch(e.target.value)}/>
            </div>

            <DataTable
                columns={props.columns}
                data={data}
                pagination
                paginationServer
                paginationTotalRows={totalRows}
                onChangeRowsPerPage={handlePerRowsChange}
                onChangePage={handlePageChange}
            />
        </div>
    );
};


export default withRouter(MyDataTable);