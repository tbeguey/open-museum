import React from 'react';
import OpenMuseum from './component/OpenMuseum'
import 'tailwindcss/dist/tailwind.min.css'

function App() {
  return (
    <div className="flex flex-col min-h-screen bg-gray-200">
      <OpenMuseum/>
    </div>
  );
}

export default App;
