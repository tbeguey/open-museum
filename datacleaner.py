import csv
import re
import sys


def main():
    with open('out.txt', encoding="ISO-8859-1") as csvfile:
        res = csv.reader(csvfile, delimiter='\t')
        res = list(res)
        print(res[0])
        for row in res:
            rejected = False
            if row[0]:
                # Domaine
                domaines = {'dessin', 'peinture', 'estampe', 'archéologie', 'photographie', 'céramique'}
                if row[0] not in domaines:
                    rejected = True

            if not rejected:

                if row[1]:
                    # denomination
                    if not row[1]:
                        # prendre la valeur à droite du point virgule
                        split = row[0].split(';')
                        if len(split) == 1:
                            row[1] = split[1]
                    else:
                        # si pas vide prendre ce qui est à gauche du point virgule
                        row[1] = row[1].split(';')[0]
                if row[2]:
                    # appelation
                    row[2] = row[2].split(';')[0]
                if row[3]:
                    # titre
                    row[3] = row[3].lower()
                    row[3] = row[3].split('(')[0]
                    row[3] = row[3].split(';')[0]
                    row[3] = row[3].split('/')[0]
                    row[3] = row[3].split('?')[0]
                if row[4]:
                    # auteur
                    row[4] = row[4].lower()
                    row[4] = row[4].split('(')[0]
                    row[4] = row[4].split(';')[0]
                    row[4] = row[4].split('/')[0]
                    row[4] = row[4].split('?')[0]
                if row[5]:
                    # millesime
                    if row[5]:
                        row[5] = row[5][0:4]
                if row[6]:
                    # dimension
                    row[6] = re.findall(r'-?\d+\.?\d*', row[6])
                if row[7]:
                    # Materiaux_techniques
                    row[7] = row[7].split(';')
                    for key, r in enumerate(row[7]):
                        row[7][key] = r.split('(')
                if row[9]:
                    # lieu de conservation
                    print(row[9])
                    s = row[9].split(';')
                    if len(s) == 2:
                        row[9] = s[1]
                if row[10]:
                    # pays
                    row[10] = row[10].split(';')[0]
                    row[10] = row[10].split('(')[0]
                if row[12]:
                    # geolocalisation
                    row[12] = row[12].split(',')

                    # TODO insert in base
                    print(row)


if __name__ == "__main__":
    sys.exit(main())
