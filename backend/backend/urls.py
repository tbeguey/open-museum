from django.conf.urls import url, include
from django.contrib import admin
from rest_framework import routers

from api.views import *

router = routers.DefaultRouter()
router.register(r"countries", CountryViewSet)
router.register(r"cities", CityViewSet)
router.register(r"musea", MuseumViewSet)
router.register(r"materials-techniques", MaterialOrTechniqueViewSet)
router.register(r"categories", CategoryViewSet)
router.register(r"artworks", ArtworkViewSet)
router.register(r"authors", AuthorViewSet)


urlpatterns = [
    url(r"^", include(router.urls)),
    url(r"^admin/", admin.site.urls),
]
