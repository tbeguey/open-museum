# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from rest_framework.viewsets import ReadOnlyModelViewSet
from rest_framework import permissions

from .models import *
from .serializers import *
from .filters import *


class CountryViewSet(ReadOnlyModelViewSet):
    queryset = Country.objects.all()
    serializer_class = CountrySerializer
    permission_classes = [
        permissions.AllowAny,
    ]


class CityViewSet(ReadOnlyModelViewSet):
    queryset = City.objects.all()
    serializer_class = CitySerializer
    permission_classes = [
        permissions.AllowAny,
    ]
    filterset_class = CityFilter


class MuseumViewSet(ReadOnlyModelViewSet):
    queryset = Museum.objects.all()
    serializer_class = MuseumSerializer
    permission_classes = [
        permissions.AllowAny,
    ]
    filterset_class = MuseumFilter


class MaterialOrTechniqueViewSet(ReadOnlyModelViewSet):
    queryset = MaterialOrTechnique.objects.all()
    serializer_class = MaterialOrTechniqueSerializer
    permission_classes = [
        permissions.AllowAny,
    ]


class CategoryViewSet(ReadOnlyModelViewSet):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer
    permission_classes = [
        permissions.AllowAny,
    ]


class ArtworkViewSet(ReadOnlyModelViewSet):
    queryset = Artwork.objects.all()
    serializer_class = ArtworkSerializer
    permission_classes = [
        permissions.AllowAny,
    ]
    filterset_class = ArtworkFilter


class AuthorViewSet(ReadOnlyModelViewSet):
    queryset = Author.objects.all()
    serializer_class = AuthorSerializer
    permission_classes = [
        permissions.AllowAny,
    ]
    filterset_class = AuthorFilter
