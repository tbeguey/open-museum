# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from api.models import *

admin.site.register(Artwork)
admin.site.register(Country)
admin.site.register(City)
admin.site.register(Museum)
admin.site.register(Author)
admin.site.register(MaterialOrTechnique)
admin.site.register(Category)

