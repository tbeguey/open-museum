from rest_framework.serializers import ModelSerializer

from .models import *


class CountrySerializer(ModelSerializer):
    class Meta:
        model = Country
        fields = "__all__"


class CitySerializer(ModelSerializer):
    class Meta:
        model = City
        fields = "__all__"


class MuseumSerializer(ModelSerializer):
    city = CitySerializer(read_only=True)
    
    class Meta:
        model = Museum
        fields = "__all__"


class MaterialOrTechniqueSerializer(ModelSerializer):
    class Meta:
        model = MaterialOrTechnique
        fields = "__all__"


class CategorySerializer(ModelSerializer):
    class Meta:
        model = Category
        fields = "__all__"



class AuthorSerializer(ModelSerializer):
    class Meta:
        model = Author
        fields = "__all__"


class ArtworkSerializer(ModelSerializer):
    author = AuthorSerializer(read_only=True)
    exhibition_museum = MuseumSerializer(read_only=True)

    class Meta:
        model = Artwork
        fields = "__all__"