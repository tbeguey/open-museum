import django_filters
from django_filters.rest_framework import filterset

from api.models import *


class ArtworkFilter(django_filters.FilterSet):
    museum = django_filters.NumberFilter(method="for_museum")
    author = django_filters.ModelChoiceFilter(queryset=Author.objects.all())
    categories = django_filters.ModelChoiceFilter(
        queryset=Category.objects.all()
    )
    materials_or_techniques = django_filters.ModelChoiceFilter(
        queryset=MaterialOrTechnique.objects.all()
    )
    search = django_filters.CharFilter(method="search_filter")

    def for_museum(self, queryset, name, value):
        return queryset.filter(exhibition_museum=value)

    def search_filter(self, queryset, name, value):
        return queryset.filter(title__icontains=value)

    class Meta:
        model = Artwork
        fields = ["museum", "author", "categories", "materials_or_techniques", "search"]


class AuthorFilter(django_filters.FilterSet):
    search = django_filters.CharFilter(method="search_filter")

    def search_filter(self, queryset, name, value):
        return queryset.filter(name__icontains=value)

    class Meta:
        model = Author
        fields = ["search",]

class MuseumFilter(django_filters.FilterSet):
    search = django_filters.CharFilter(method="search_filter")

    def search_filter(self, queryset, name, value):
        return queryset.filter(name__icontains=value)

    class Meta:
        model = Museum
        fields = ["search",]

class CityFilter(django_filters.FilterSet):
    search = django_filters.CharFilter(method="search_filter")
 
    def search_filter(self, queryset, name, value):
        return queryset.filter(name__icontains=value)

    class Meta:
        model = City
        fields = ["search",]