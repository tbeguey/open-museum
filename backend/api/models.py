# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models


class Country(models.Model):
    name = models.CharField(max_length=256)


class City(models.Model):
    name = models.CharField(max_length=256)
    country = models.ForeignKey(
        to=Country, on_delete=models.SET_NULL, null=True, blank=True
    )
    longitude = models.DecimalField(
        max_digits=9, decimal_places=6, null=True, blank=True
    )
    latitude = models.DecimalField(
        max_digits=9, decimal_places=6, null=True, blank=True
    )


class Museum(models.Model):
    name = models.CharField(max_length=256)
    city = models.ForeignKey(to=City, on_delete=models.SET_NULL, null=True, blank=True)


class Author(models.Model):
    name = models.CharField(max_length=256)


class MaterialOrTechnique(models.Model):
    name = models.CharField(max_length=256)


class Category(models.Model):
    name = models.CharField(max_length=256)


class Artwork(models.Model):
    title = models.CharField(max_length=256)
    author = models.ForeignKey(
        to=Author, on_delete=models.SET_NULL, null=True, blank=True
    )
    creation_city = models.ForeignKey(
        to=City, on_delete=models.SET_NULL, null=True, blank=True
    )
    creation_date = models.DateField(null=True, blank=True)
    exhibition_museum = models.ForeignKey(
        to=Museum, on_delete=models.SET_NULL, null=True, blank=True
    )
    width = models.PositiveIntegerField(null=True, blank=True)
    height = models.PositiveIntegerField(null=True, blank=True)
    depth = models.PositiveIntegerField(null=True, blank=True)
    subject = models.CharField(max_length=10024, null=True, blank=True)
    theme = models.CharField(max_length=256, null=True, blank=True)
    materials_or_techniques = models.ManyToManyField(to=MaterialOrTechnique, blank=True)
    categories = models.ManyToManyField(to=Category, blank=True)
