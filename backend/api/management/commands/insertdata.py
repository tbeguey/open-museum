import csv
import re
import datetime
from django.core.management.base import BaseCommand, CommandError

from api.models import *


class Command(BaseCommand):
    help = "Insert the csv datas in the database"

    def handle(self, *args, **options):
        category = None
        author = None
        matsortechs = []
        country = None
        city = None
        museum = None
        width = None
        height = None
        artwork = None

        with open("out.txt", encoding="ISO-8859-1") as csvfile:
            res = csv.reader(csvfile, delimiter="\t")
            res = list(res)
            for row in res:
                rejected = False
                if row[0]:
                    # Domaine
                    domaines = {
                        "dessin",
                        "peinture",
                        "estampe",
                        "archéologie",
                        "photographie",
                        "céramique",
                    }
                    if row[0] not in domaines:
                        rejected = True

                if not rejected:

                    if row[1]:
                        # denomination
                        if not row[1]:
                            # prendre la valeur à droite du point virgule
                            split = row[0].split(";")
                            if len(split) == 1:
                                row[1] = split[1]
                        else:
                            # si pas vide prendre ce qui est à gauche du point virgule
                            row[1] = row[1].split(";")[0]
                    if row[2]:
                        # appelation
                        row[2] = row[2].split(";")[0]
                    if row[3]:
                        # titre
                        row[3] = row[3].lower()
                        row[3] = row[3].split("(")[0]
                        row[3] = row[3].split(";")[0]
                        row[3] = row[3].split("/")[0]
                        row[3] = row[3].split("?")[0]
                    if row[4]:
                        # auteur
                        row[4] = row[4].lower()
                        row[4] = row[4].split("(")[0]
                        row[4] = row[4].split(";")[0]
                        row[4] = row[4].split("/")[0]
                        row[4] = row[4].split("?")[0]
                    if row[5]:
                        # millesime
                        if row[5]:
                            row[5] = row[5][0:4]
                    if row[6]:
                        # dimension
                        row[6] = re.findall(r"-?\d+\.?\d*", row[6])
                    if row[7]:
                        # Materiaux_techniques
                        row[7] = row[7].split(";")
                        for key, r in enumerate(row[7]):
                            row[7][key] = r.split("(")
                    if row[9]:
                        # lieu de conservation
                        s = row[9].split(";")
                        if len(s) == 2:
                            row[9] = s[1]
                    if row[10]:
                        # pays
                        row[10] = row[10].split(";")[0]
                        row[10] = row[10].split("(")[0]
                    if row[12]:
                        # geolocalisation
                        row[12] = row[12].split(",")

                        # 0 - Domaine
                        # 1 - Dénomination OSEF
                        # 2 - Appellation OSEF
                        # 3 - Titre
                        # 4 - Auteur
                        # 5 - Millésime
                        # 6 - Dimension []
                        # 7 - Matériaux techniques []
                        # 8 - Sujet
                        # 9 - Lieu de conservation
                        # 10 - Ecole (Pays)
                        # 11 - Ville
                        # 12 - Géolocalisation ville []

                        category, _ = Category.objects.get_or_create(name=row[0])
                        author, _ = Author.objects.get_or_create(name=row[4])
                        if type(row[7]) is list:
                            matsortechs = [
                                MaterialOrTechnique.objects.get_or_create(name=m)[0] for m in row[7][0]
                            ]
                        else:
                            matsortechs = [MaterialOrTechnique.objects.get_or_create(name=row[7])[0]]
                        country, _ = Country.objects.get_or_create(name=row[10])
                        city, _ = City.objects.get_or_create(name=row[11], country=country)
                        museum, _ = Museum.objects.get_or_create(name=row[9], city=city)
        
                        try:
                            height = int(row[6][0].split('.')[0])
                        except IndexError:
                            if len(row[6]) > 1:
                                height = int(row[6][0])
                            else:
                                height = None 

                        try:
                            width = int(row[6][1].split('.')[0])
                        except IndexError:
                            if len(row[6]) > 1:
                                width = int(row[6][1])
                            else:
                                width = None 

                        artwork, _ = Artwork.objects.get_or_create(
                            title=row[3],
                            author=author,
                            creation_date=datetime.date(int(row[5]), 1, 1) if row[5] else None,
                            height=height,
                            width=width,
                            subject=row[8],
                            exhibition_museum=museum,
                        )
                        artwork.categories.add(category)
                        artwork.materials_or_techniques.set(matsortechs)
                        #exit()
